#!/bin/sh

export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-

export PROJECT_DIR=/home/oser/github/cubieboard
export LINUX_DIR=$PROJECT_DIR/linux-sunxi
export BUSYBOX_DIR=$PROJECT_DIR/busybox-sunxi
export UBOOT_DIR=$PROJECT_DIR/u-boot-sunxi
export OUTPUT=$PROJECT_DIR/output
export ROOTFS=$PROJECT_DIR/rootfs
export TOOLS=$PROJECT_DIR/tools

# for linux kernel
export INSTALL_MOD_PATH=$OUTPUT/rootfs
export INSTALL_HDR_PATH=$OUTPUT/rootfs

mkdir -p $ROOTFS
