#!/bin/bash

echo $ARCH

$TOOLS/mkinitrdimg

qemu-system-arm -M cubieboard -m 512M -display none -serial stdio -kernel   \
$LINUX_DIR/arch/arm/boot/zImage -dtb  \
$LINUX_DIR/arch/arm/boot/dts/sun4i-a10-cubieboard.dtb -initrd  $OUTPUT/initrd.img.gz -append "root=/dev/ram rdinit=linuxrc initcall_debug=1" 
