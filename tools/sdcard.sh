#!/bin/bash

# start   sector size                usage
#  0KB      0     8KB              Unused, availiable for an MBR or (limited) GPT partition table
#  8KB      16    32KB             Initial SPL loader
#  40KB     80     -                U-Boot proper
#

BOOTLOADER_BIN=../u-boot-sunxi/u-boot-sunxi-with-spl.bin

card=/dev/sdb
p=""

function sdcard_clean()
{
  dd if=/dev/zero of=${sdcard} bs=1M count=1
}

function write_bootloader()
{
  dd if=${BOOTLOADER_BIN} of=${card} bs=1024 seek=8
  echo "Write u-boot OK!"
}


function part_card()
{
  blockdev --rereadpt ${card}
  cat <<EOT | sfdisk ${card}
1M,16M,c
,,L
EOT

  sync

  mkfs.vfat ${card}${p}1
  mkfs.ext4 ${card}${p}2

}

write_bootloader
part_card




