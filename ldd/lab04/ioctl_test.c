#include <stdio.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
  /* "mknod /dev/mydev c 202 0" to create /dev/mydev */

  if (argc < 2) {
    printf("ioctl_test /dev/xxx");
    return 1;
  }

  int my_dev = open(argv[1], 0);

  if (my_dev < 0) {
    printf("Fail to open device file: %s.", argv[1]);
  } else {
    ioctl(my_dev, 100, 110);
    close(my_dev);
  }

  return 0;

}
