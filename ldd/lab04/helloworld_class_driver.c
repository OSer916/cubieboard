#include <linux/module.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/fs.h>


#define MY_MAJOR_NUM  202

#define CLASS_NAME "hello_class"
#define DEVICE_NAME "hello_device"

dev_t dev;
static struct cdev my_dev;
static struct class *helloClass = NULL;
static struct device *helloDevice = NULL;

static int my_dev_open(struct inode *inode, struct file *file)
{
  pr_info("my_dev_open() is called.\n");
  pr_info("my_dev == %p", &my_dev);
  pr_info("inode->i_cdev=%p", inode->i_cdev);

  return 0;
}

static int my_dev_close(struct inode *inode, struct file *file)
{
  pr_info("my_dev_close() is called\n");

  return 0;
}

static long my_dev_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
  pr_info("my_dev_ioctl() is called. cmd = %d, arg = %d\n", cmd, arg);

  return 0;
}


static const struct file_operations my_dev_fops = {
  .owner = THIS_MODULE,
  .open  = my_dev_open,
  .release = my_dev_close,
  .unlocked_ioctl = my_dev_ioctl,
};


static int __init hello_init(void)
{
  int ret;

  dev = MKDEV(MY_MAJOR_NUM, 0);
  pr_info("Hello World init\n");

  /* Allocate device numbers */
  ret = register_chrdev_region(dev, 1, "my_char_device");
  if (ret < 0) {
    pr_info("Unbale to allocate mayor number %d\n", MY_MAJOR_NUM);
    return ret;
  }

  cdev_init(&my_dev, &my_dev_fops);
  ret = cdev_add(&my_dev, dev, 1);
  if (ret < 0) {
    unregister_chrdev_region(dev, 1);
    return ret;
 }

  helloClass = class_create(THIS_MODULE, CLASS_NAME);
  if (IS_ERR(helloClass)) {
    unregister_chrdev_region(dev, 1);
    pr_info("Failed to register device class\n");
    return PTR_ERR(helloClass);
  }

  pr_info("device class registered correctly\n");
  
  helloDevice = device_create(helloClass, NULL, dev, NULL, DEVICE_NAME);
  if (IS_ERR(helloDevice)) {
    class_destroy(helloClass);
    unregister_chrdev_region(dev, 1);
    pr_info("Failed to create the device\n");
    return PTR_ERR(helloDevice);
  }

  pr_info("The device is created correctly\n");

  return 0;
}


static void __exit hello_exit(void)
{
  pr_info("Hello World exit\n");
  device_destroy(helloClass, dev);
  class_unregister(helloClass);
  class_destroy(helloClass);
  cdev_del(&my_dev);
  unregister_chrdev_region(MKDEV(MY_MAJOR_NUM,0), 1);
}


module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("OSer");
MODULE_DESCRIPTION("This is a module that interacts with the ioctl system call");
